import {GetterTree} from 'vuex';
import {TStateType} from './index';

export const getters: GetterTree<TStateType, TStateType> = {
  linesNumbers: (state) => Object.keys(state.linesMap),
  selectedLineStations: ({ linesMap, selectedLineNumber, data }) => {
    if (!selectedLineNumber) return [];

    const keyMap = new Map<string, number>();
    for (const index of linesMap[selectedLineNumber]) {
      keyMap.set(data[index].stop, data[index].order);
    }

    return Array.from(keyMap.entries())
      .sort(([, a], [, b]) => a - b)
      .map(([name]) => name);
  },
  selectedStationTimetable: ({ selectedLineStation, selectedLineNumber, linesMap, data }: TStateType) => {
    if (!selectedLineStation || !selectedLineNumber) return [] as string[];

    return linesMap[selectedLineNumber]
      .filter((i) => data[i].stop === selectedLineStation)
      .map((i) => data[i].time)
      .sort((a, b) => a.localeCompare(b));
  },
  filteredStations: ({ data, busStopsFilter }) => {
    const result = new Set<string>(
      data.filter(({ stop }) => stop.toLowerCase().includes(busStopsFilter.toLowerCase()))
        .map(({ stop }) => stop)
    );

    return Array.from(result).sort((a, b) => b.localeCompare(a));
  },
};
