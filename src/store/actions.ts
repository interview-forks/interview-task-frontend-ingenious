import { ActionTree } from 'vuex';
import { TStateType } from './index';
import { debounce } from '@/services/debounce';

export const actions: ActionTree<TStateType, TStateType> = {
  debouncedSetBusStopFilter: debounce(({ commit }, payload: string) => {
    commit("setBusStopFilter", payload);
  }, 500),
  async loadData({ commit }) {
    try {
      const res = await fetch("http://localhost:3000/stops", {
        method: "GET",
        headers: {
          Accept: "application/json",
        },
      });
      const data = await res.json();
      commit("setInitialData", data);
    } catch (error) {
      console.error("Error fetching data:", error);
    }
  },
};
