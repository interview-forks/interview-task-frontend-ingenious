import { createStore } from "vuex";
import { getters } from "@/store/getters";
import { mutations } from "@/store/mutations";
import { actions } from "@/store/actions";

export type TDataType = {
  line: number;
  stop: string;
  order: number;
  time: string;
}

export type TLinesMap = Record<string, number[]>;

export type TStateType = {
  data: TDataType[];
  linesMap: TLinesMap;
  selectedLineNumber: number | null;
  selectedLineStation: string;
  busStopsFilter: string;
}

const state: TStateType = {
  data: [],
  linesMap: {},
  selectedLineNumber: null,
  selectedLineStation: "",
  busStopsFilter: "",
};

const store = createStore<TStateType>({
  state,
  getters,
  mutations,
  actions,
});

export default store;