import { MutationTree } from 'vuex';
import { TDataType, TStateType } from './index';

export const mutations: MutationTree<TStateType> = {
  setBusStopFilter: (state, payload) => {
    state.busStopsFilter = payload;
  },
  setLineNumber: (state, payload) => {
    state.selectedLineNumber = payload;
    state.selectedLineStation = "";
  },
  setStation: (state, payload) => {
    state.selectedLineStation = payload;
  },
  setInitialData: (state, payload: TDataType[]) => {
    const linesMap = payload.reduce((acc, el, index) => {
      acc[el.line] = acc[el.line] || [];
      acc[el.line].push(index);
      return acc;
    }, {} as Record<string, number[]>);

    state.data = payload;
    state.linesMap = linesMap;
  },
};
